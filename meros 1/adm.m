function [b,delta] = adm(x)

M = 4;
K = 1.5;
delta_init = 0.02;
y = interp(x,M);
b = zeros(length(y),1);
delta = zeros(length(y),1);
delta(1) = delta_init;
xq = 0;

for i=1:length(y)
    if (y(i) >= xq)
        b(i) = 1;
        if (i>1)
            if (b(i)==b(i-1))
                delta(i) = delta(i-1)*K;
            else
                delta(i) = delta(i-1)*K^(-1);
            end
        end
        xq = xq + b(i)*delta(i);
    else
        b(i) = -1;
        if (i>1)
            if (b(i)==b(i-1))
                delta(i) = delta(i-1)*K;
            else
                delta(i) = delta(i-1)*K^(-1);
            end
        end
        xq = xq + b(i)*delta(i);
    end
end
            
        
        